function Resolve-GitBookConfigurationFilePath {
    [CmdletBinding()]
    param (
        [string]$ConfigurationFilePath
    )
    
    begin {
    }
    
    process {
        If ([string]::IsNullOrEmpty($ConfigurationFilePath)) {
            $DiscoveredConfigurationFiles = Resolve-Path './docs/book.json','./book.json' -ErrorAction SilentlyContinue
            If ($DiscoveredConfigurationFiles.Count -eq 0) {
                $NoConfigurationFileFoundMessage = @(
                    "No GitBook configuration file (book.json) specified, no configuration file found in default paths:"
                    "    ./docs/book.json"
                    "    ./book.json"
                    "Please specify the path to a valid gitbook configuration file."
                ) -join "`r`n"
                Throw $NoConfigurationFileFoundMessage
            } ElseIf ($DiscoveredConfigurationFiles.Count -gt 1) {
                $AmbiguousConfigurationFileMessage = @(
                    "No GitBook configuration file (book.json) specified, configuration files found in both default paths:"
                    "    ./docs/book.json"
                    "    ./book.json"
                    "Please specify one configuration file to update."
                ) -join "`r`n"
                Throw $AmbiguousConfigurationFileMessage
            } Else {
                [string]$DiscoveredConfigurationFiles
            }
        } Else {
            Try {
                [string](Resolve-Path -Path $ConfigurationFilePath -ErrorAction Stop)
            } Catch {
                Throw $PSItem.Exception
            }
        }
    }
    
    end {
    }
}