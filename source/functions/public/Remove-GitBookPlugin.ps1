function Remove-GitBookPlugin {
  <#
    .SYNOPSIS
      Remove a gitbook plugin to the project's documentation config
    .DESCRIPTION
      Remove a gitbook plugin to the project's documentation configuration.
    .PARAMETER PluginName
      The name(s) of the plugin(s) to be removed from the config file.
      This function does not verify whether or not the plugin is valid or discoverable.

      Note that this command _does_ modify the pluginConfig setting where individual plugins are configured.
      It removes the plugin to the load list and from the pluginConfig both.
    .PARAMETER ConfigurationFilePath
      The path to the GitBook configuration file.
      Will search for the file in the default locations if not specified.
    .EXAMPLE
      Remove-GitBookPlugin -PluginName code
      Removes the code plugin from the default GitBook config in the project.
    .EXAMPLE
      'code', 'code-captions' | Remove-GitBookPlugin
      Removes the code and code-captions plugins from the default GitBook config in the project, showing passing plugins by the pipeline.
    .EXAMPLE
      Remove-GitBookPlugin -PluginName code -ConfigurationFilePath ./example/book.json
    .INPUTS
      String{}
      One or more strings representing the plugins to be removed.
    .OUTPUTS
      String
      JSON string representation of the GitBook config after updating.
    .NOTES
      Alias: rgbp
  #>
  [CmdletBinding()]
  param (
    [Alias('Name','Plugin')]
    [Parameter(ValueFromPipeline=$true)]
    [string[]]$PluginName,
    [Alias('Path','FilePath','Config')]
    [string]$ConfigurationFilePath
  )
  
  begin {
    If ([string]::IsNullOrEmpty($ConfigurationFilePath)) {
      $ConfigurationFilePath = Resolve-GitBookConfigurationFilePath
    } Else {
      $ConfigurationFilePath = Resolve-GitBookConfigurationFilePath -ConfigurationFilePath $ConfigurationFilePath
    }
    $Configuration = Get-Content $ConfigurationFilePath -ErrorAction Stop | ConvertFrom-Json -ErrorAction Stop
  }
  
  process {
    ForEach ($Plugin in $PluginName) {
      If ($Plugin -in $Configuration.Plugins) {
        $Configuration.Plugins = $Configuration.Plugins | Where-Object -FilterScript {$PSItem -ne $Plugin}
        # Because the config is returned as a PSCustomObject we need to use PSObject.Properties
        # instead of recursing through the hash and removing the key. This works roughly the same.
        If ($Configuration.PluginsConfig.PSObject.Properties.Name -contains $Plugin) {
          $Configuration.PluginsConfig.PSObject.Properties.Remove($Plugin)
        }
      } Else {
        Write-Warning "GitBook Plugin [$Plugin] not found in configuration"
      }
    }
  }
  
  end {
    $Configuration | ConvertTo-Json -OutVariable ConfigurationJson | Out-File $ConfigurationFilePath
    $ConfigurationJson
  }
}

Set-Alias -Name rgbp -Value Remove-GitBookPlugin