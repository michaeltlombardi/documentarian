function New-Document {
  
  <#
    .SYNOPSIS
    Create new markdown docs in the docs folder.
    
    .DESCRIPTION
    Scaffold new markdown documents in the project with headers and subchapters.

    .PARAMETER Title
    The title of the document to be created.
    Optionally, you can specify the title to include both the type and chapters, such as 'Concept/Chapter/Subchapter/My Document Title'

    .PARAMETER Type
    The type of documentation to be created, usually concept, narrative, or reference.
    Determines which folder inside the docs directory to place the subchapters and document.

    .PARAMETER Chapter
    Arbitrary subchapters for the document to reside within, separated by a forward slash.
    
    .EXAMPLE
    New-Document -Type Narrative -Title "Using Helper Functions" -Chapter "Workflow"

    This example shows the most explicit use of the command to create a markdown document at `./docs/narrative/Workflow/using-helper-functions.md`

    .EXAMPLE
    New-Document -Type Concept -Title "Writing Helper Functions" -Chapter "Contributing/Code"

    This example shows using the command to create a markdown document in subchapters at `./docs/concept/Contributing/Code/writing-helper-functions.md`

    .EXAMPLE
    New-Document -Title "Concept/Contributing/Code/Writing Helper Functions"

    In this example we use the shorthand specification in the title, including the type of documentation and chapters prepended to the title.

    .EXAMPLE
    'Concept/Contributing/Code/Writing Helper Functions','Narrative/Workflow/Using Helper Functions" `
    | New-Document

    Here we show that you can pass multiple titles in the shorthand format to the command over the pipeline.

  #>

  [CmdletBinding()]
  param (
    [Parameter(Position=0, ValueFromPipeline=$true)]
    [string]$Title,
    [string]$Type,
    [string]$Chapter
  )
  
  begin {
    If (!(Test-Path ./docs)) {
      Throw "Could not find docs folder; command must be run from root of project with a docs folder."
    }
  }
  
  process {
    # Determine whether or not to parse the title for the type and chapter:
    $TypeNotSpecified    = $Type    -in    @($null, "")
    $ChapterNotSpecified = $Chapter -in    @($null, "")
    $TitleIncludesType   = $Title   -match '^\w+/'
    If ($TitleIncludesType -and $TypeNotSpecified -and $ChapterNotSpecified) {
      # If correctly formatted, title can then be split to retrieve from Type/Chapter/Title
      $SplitTitle = $Title.Split('/')
      $Type       = $SplitTitle[0]
      $Title      = $SplitTitle[-1]
      # Chapters aren't necessary, so if they're not included, don't make a goofy layout.
      If ($SplitTitle.Count -gt 2) {
        $Chapter    = $SplitTitle[1..($SplitTitle.Count - 2)] -join '/'
      } Else {
        $Chapter = ""
      }
    }

    $DocumentFolder = "./docs/$Type/$Chapter"
    $DocumentName   = $Title.Replace(' ','-').ToLower()
    $DocumentPath   = "$DocumentFolder/$DocumentName.md"
    If (!(Test-Path $DocumentFolder)) {
      New-Item -Path $DocumentFolder -ItemType Directory -Force | Out-Null
    }
    If (!(Test-Path -Path $DocumentPath)) {
      New-Item -Path $DocumentPath -ItemType File -Value "# $Title"
    }

    # Cleanup Type and Chapter for pipeline
    $Type    = ""
    $Chapter = ""
  }
  
  end {
  }
}
