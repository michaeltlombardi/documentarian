function Add-GitBookPlugin {
  <#
    .SYNOPSIS
    Add a gitbook plugin to the project's documentation config
    .DESCRIPTION
    Add a gitbook plugin to the project's documentation configuration.
    You can specify a '-' at the beginning of the plugin name to remove a built-in plugin.
    .PARAMETER PluginName
    The name(s) of the plugin(s) to be added to the config file.
    This function does not verify whether or not the plugin is valid or discoverable.
    
    If you specify a '-' at the beginning of the plugin name for a default plugin it will instead remove that plugin from the load order.
    This only works for built-in plugins.

    Note that this command does _not_ modify the pluginConfig setting where individual plugins are configured.
    It adds the plugin to the load list and will therefore get the plugin's default config.
    .PARAMETER ConfigurationFilePath
    The path to the GitBook configuration file.
    Will search for the file in the default locations if not specified.
    .EXAMPLE
    Add-GitBookPlugin -PluginName code
    Adds the code plugin to the default GitBook config in the project.
    .EXAMPLE
    'code', 'code-captions' | Add-GitBookPlugin
    Adds the code and code-captions plugins to the default GitBook config in the project, showing passing plugins by the pipeline.
    .EXAMPLE
    Add-GitBookPlugin -PluginName code -ConfigurationFilePath ./example/book.json
    .INPUTS
    String[]
    One or more strings representing the plugins to be added.
    .OUTPUTS
    String
    JSON string representation of the GitBook config after updating.
    .NOTES
    Alias: agbp
  #>
  [CmdletBinding()]
  param (
    [Alias('Name','Plugin')]
    [Parameter(ValueFromPipeline=$true)]
    [string[]]$PluginName,
    [Alias('Path','FilePath','Config')]
    [string]$ConfigurationFilePath
  )
  
  begin {
    If ([string]::IsNullOrEmpty($ConfigurationFilePath)) {
      $ConfigurationFilePath = Resolve-GitBookConfigurationFilePath
    } Else {
      $ConfigurationFilePath = Resolve-GitBookConfigurationFilePath -ConfigurationFilePath $ConfigurationFilePath
    }
    $Configuration = Get-Content $ConfigurationFilePath -ErrorAction Stop | ConvertFrom-Json -ErrorAction Stop
  }
  
  process {
    ForEach ($Plugin in $PluginName) {
      If ($Plugin -notin $Configuration.Plugins) {
        $Configuration.Plugins += $Plugin
      } Else {
        Write-Warning "GitBook Plugin [$Plugin] already found in configuration"
      }
    }
  }
  
  end {
    $Configuration | ConvertTo-Json -OutVariable ConfigurationJson | Out-File $ConfigurationFilePath
    $ConfigurationJson
  }
}

Set-Alias -Name agbp -Value Add-GitBookPlugin