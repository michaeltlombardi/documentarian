# Adding Media Files to Your Documentation

When writing documentation, it's often helpful to include different formats for the people relying on your documentation.
Some people learn best from visual cues, or from video walkthroughs, or audio files, or other media.
Sometimes you might want to include example scripts or template files or similar without putting them into the body of the documentation.

The pattern that the documentarian template assumes is that you'll place those media files into a folder in your project and reference them from your markdown files wherever necessary.

## Images

Markdown includes out-of-the-box support for image files in documents.
For example, here's an image of the folder structure of this project (not fully expanded):

![Unexpanded Project Folder Structure](../media/unexpanded-folder-structure.PNG)

That image was placed in our markdown with this code:

```markdown
![Unexpanded Project Folder Structure](../media/unexpanded-folder-structure.PNG)
```

Essentially, as with HTML images, you need to make sure you include the _relative path_ from your documentation file to the media file you want to reference.

## Video Files

Markdown does _not_ include native support for embedding video files except as links to them.
However, there is a [GitBook plugin called `html5-video`](https://github.com/Harris2012/gitbook-plugin-html5-video) that does.

### Configuration

You can add support for video files by specifying the plugin in your project's `book.json` file:

```json
// Before adding the plugin:
{
    "title": "Documentarian",
    "description": "Scaffolding for documentation of your PowerShell projects",
    "author": "Michael Lombardi (michael.t.lombardi@outlook.com)",
    "language": "en",
    "plugins": ["expandable-chapters-small"]
}
// After adding the plugin:
{
    "title": "Documentarian",
    "description": "Scaffolding for documentation of your PowerShell projects",
    "author": "Michael Lombardi (michael.t.lombardi@outlook.com)",
    "language": "en",
    "plugins": [
        "expandable-chapters-small",
        "html5-video"
    ]
}
```

### Use

In order to embed video files in your documentation with this plugin, you'll have to reference them.
For example:

```markdown
{% video %}../media/example-video.webm{% endvideo %}
```

In this file, that gets the following result:

{% video %}../media/example-video.webm{% endvideo %}

For more information on embedding videos with this plugin, you'll probably want to [read the plugin's documentation](https://github.com/Harris2012/gitbook-plugin-html5-video)

## Audio Files

You can use the [same plugin for audio as you can for video:](#Configuration)

For example, the following markdown will generate an audio-player:

```markdown
{% video height="0" %}../media/example-audio.m4a{% endvideo %}
```

Which you can see here:

{% video height="0" %}../media/example-audio.m4a{% endvideo %}

Note, however, that you _must specify_ a height of zero for your audio files.
If you do not specify a height, your audio file will take up vertical space in your document.
For example:

{% video %}../media/example-audio.m4a{% endvideo %}

## Wrapping Up
In summary, to support videos and audio files in your documentation you must add the `html5-video` plugin.
If you're using this module at a version of `0.8.0` or higher, you have the option to include this plugin in your config file when using the included plaster template.

You can also use the `Add-GitBookPlugin` function from this module to update your configuration file.

```powershell
# Make sure to update the ConfigurationFilePath with the _actual_ path to your config file!
# If you used documentarian to scaffold your docs and you're running this from the root of
# your project, the example here should work for you. If it doesn't, that's a bug!
Add-GitBookPlugin -Plugin 'html5-video'
```