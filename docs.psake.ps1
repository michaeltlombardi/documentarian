#!/usr/bin/env pwsh
# PSake makes variables declared here available in other scriptblocks
# Init some things
Properties {
  $ProjectRoot       = $env:BHProjectPath
  $Commands          = @('npm','git')
  $Timestamp         = Get-date -uformat "%Y%m%d-%H%M%S"
  $ApiKey            = $env:APIKEY
  $CompilingFolder   = "$env:BHProjectPath/compiled_docs"
  $OutputPdfPath     = "$ProjectRoot/$env:BHProjectName.pdf"
  $OutputSitePath    = "$ProjectRoot/public"
  $PathUpdateScript  = {
      foreach($level in "Machine","User") {
          [Environment]::GetEnvironmentVariables($level).GetEnumerator() | % {
             # For Path variables, append the new values, if they're not already in there
             if($_.Name -match 'Path$') { 
                $_.Value = ($((Get-Content "Env:$($_.Name)") + ";$($_.Value)") -split ';' | Select -unique) -join ';'
             }
             $_
          } | Set-Content -Path { "Env:$($_.Name)" }
      }
  }
}

Task DeterminePlatform {
  If ($PSVersionTable.PSVersion.Major -lt 6 -or $PSVersionTable.Platform -match '^Win') {
      $Script:Platform = "Windows"
      . $PathUpdateScript
  } Else {
      $Script:Platform = $PSVersionTable.OS.split(' ')[0]
  }
  Write-Verbose "Building on $Script:Platform"
  # On CentOS the executable for node is 'node' - on Ubuntu it's 'nodejs' so we need to alias that here.
  If (Get-Command nodejs -ErrorAction SilentlyContinue) {
      New-Alias -Name node -Value nodejs -Scope Global -ErrorAction SilentlyContinue
  }
}

Task InstallPrerequisites -depends DeterminePlatform {
  $Script:NodeModulesPath = "$env:BHProjectPath/node_modules/"
  $Script:GitBookCli      = "$Script:NodeModulesPath/gitbook-cli/bin/gitbook.js"
  $Script:GitBookSummary  = "$Script:NodeModulesPath/gitbook-summary/bin/summary.js"
  If (Get-Command -Name npm -CommandType Application -ErrorAction SilentlyContinue) {
      # Only try installing if NPM is found and only then if either of the executables are not.
      $GitBookCliInstalled = [bool](Get-Command -Name $NodeModulesPath/.bin/gitbook -ErrorAction SilentlyContinue)
      $GitBookSummaryInstalled = [bool](Get-Command -Name $NodeModulesPath/.bin/book -ErrorAction SilentlyContinue)
      If (!$GitBookCliInstalled -or !$GitBookSummaryInstalled) {
          If ($pwd -ne $env:BHProjectPath) { Push-Location $env:BHProjectPath }
          npm install $env:BHProjectPath
      }
  } Else {
      Throw "Could not find npm in the PATH. NPM is required to install gitbook."
  }
}

Task Clean {
  If (Test-Path $CompilingFolder) {
      Remove-Item $CompilingFolder -Recurse -Force
  }
  If (Test-Path $OutputSitePath) {
      Remove-Item $OutputSitePath -Recurse -Force
  }
  If (Test-Path $OutputPdfPath) {
      Remove-Item $OutputPdfPath
  }
}

Task Compile -depends Clean, InstallPrerequisites {
  $null = mkdir $CompilingFolder
  Copy-Item -Path $ProjectRoot/*.md -Destination $CompilingFolder -Force
  Copy-Item -Path $ProjectRoot/docs/* -Destination $CompilingFolder -Recurse -Force
  If (Test-Path $ProjectRoot/media) {
      Copy-Item -Path $ProjectRoot/media -Destination $CompilingFolder -Recurse -Container -Force
  }
  # Compile reference documentation
  # First we have to ensure the module is not currently imported, then import it into the global scope.
  # It _must_ be global scope for PlatyPS to see it correctly in the task, otherwise it will use a system version.
  Remove-Module $env:BHProjectName -Force -ErrorAction SilentlyContinue
  Import-Module "$ProjectRoot/source/$env:BHProjectName.psd1" -Force -Verbose -Scope Global
  # Export comment-based help via PlatyPS:
  New-MarkdownHelp -Module $env:BHProjectName -AlphabeticParamsOrder -NoMetadata -OutputFolder $CompilingFolder/reference/functions -WithModulePage -Force
  # PlatyPS allows you to create  a module page but doesn't bother filling it out with the descriptions or synopses.
  # We'll replace the text manually ourselves here and rename the module page to readme for GitBook's sake.
  Rename-Item "$CompilingFolder/reference/functions/$env:BHProjectName.md" -NewName 'README.md'
  $ModulePageContent = Get-Content "$CompilingFolder/reference/functions/README.md" | Select-Object -Skip 12
  # Loop through the exported commands because PlatyPS, by default, doesn't fill out the synopsis for them on the module page.
  ForEach ($Function in (Get-Command -Module $env:BHProjectName | Select-Object -ExpandProperty Name)) {
    $FunctionSynopsis  = Get-Help $env:BHProjectName\$Function | Select-Object -ExpandProperty Synopsis
    $ModulePageContent = $ModulePageContent -replace "{{Manually Enter $Function Description Here}}", $FunctionSynopsis
  }
  # Write the module page back out.
  $ModulePageContent | Out-File "$CompilingFolder/reference/functions/README.md"
  # Remove the module since we had to add it to the global scope.
  Remove-Module $env:BHProjectName -Force -ErrorAction SilentlyContinue
  Push-Location -Path $CompilingFolder
  # Compile the GitBook summary and install plugins
  node $Script:GitBookSummary sm
  node $Script:GitBookCli install
  Pop-Location
}

Task GenerateSite -depends Compile {
  node $Script:GitBookCli build $CompilingFolder $OutputSitePath
}

Task GeneratePdf -depends Compile {
  # Ensure the calibre dependency is installed for outputting to PDF
  If (Get-Command ebook-convert -CommandType Application -ErrorAction SilentlyContinue) {
      node $Script:GitBookCli pdf $CompilingFolder $OutputPdfPath
  } Else {
      Write-Error "Could not export as PDF, utility ebook-convert not found in PATH. Please install calibre to export as PDF: https://calibre-ebook.com/download"
  }
}

Task GenerateBoth -depends GeneratePdf, GenerateSite

Task LivePreview -depends Compile {
  Push-Location -Path $CompilingFolder
  node $Script:GitBookCli serve
  Pop-Location
}
